package ar.com.uade.impl;

import ar.com.uade.tda.TDACola;

public class TDAColaImpl implements TDACola {

	private int[] valoresCola;
	private int cantidad;
	
	@Override
	public void inicializar() {
		valoresCola = new int[100];
		cantidad = 0;
	}

	@Override
	public void acolar(int valor) {
		for (int i = cantidad - 1;i >= 0;i--){
			valoresCola[i+1] = valoresCola[i];
		}
		valoresCola[0] = valor;
		cantidad++;
	}

	@Override
	public void desacolar() {
		cantidad--;
	}

	@Override
	public boolean colaVacia() {
		return cantidad == 0;
	}

	@Override
	public int primero() {
		return valoresCola[cantidad - 1];
	}

	@Override
	public void imprimir() {
		for(int i=0;i < cantidad;i++){
			System.out.println("Fila " + i + " :" + valoresCola[i]);
		}
	}

}
