package ar.com.uade.algoritmos;

import ar.com.uade.impl.TDAColaImpl;
import ar.com.uade.impl.TDAPilaImpl;
import ar.com.uade.tda.TDACola;
import ar.com.uade.tda.TDAPila;

public class EjercicioCola {

	public void pasarCola(TDACola origen, TDACola destino){
		
		while(!origen.colaVacia()){
			destino.acolar(origen.primero());
			origen.desacolar();
		}
	}
	
	public void invertirCola(TDACola origen){
		
		TDAPila pilaAux = new TDAPilaImpl();
		
		while(!origen.colaVacia()){
			pilaAux.apilar(origen.primero());
			origen.desacolar();
		}
		
		while(!pilaAux.pilaVacia()){
			origen.acolar(pilaAux.tope());
			pilaAux.desapilar();
		}
	}
	
	public void invertirColaSinPilas(TDACola origen){
		
		TDACola colaAux = new TDAColaImpl();
		colaAux.inicializar();
		int cantElemOrigen = 0;
		
		while(!origen.colaVacia()){
			colaAux.acolar(origen.primero());
			origen.desacolar();
			cantElemOrigen++;
		}
		
		while (!colaAux.colaVacia()){
			for (int i = 0;i < cantElemOrigen - 1;i++){
				colaAux.acolar(colaAux.primero());
				colaAux.desacolar();
			}
			
			origen.acolar(colaAux.primero());
			colaAux.desacolar();
			cantElemOrigen--;
		}
		
	}
	
	public boolean C1CoincideConC2(TDACola cola1, TDACola cola2){
		
		return false;
	}
	
	public boolean esCapicua(TDACola cola){
		
		TDAPila pilaAux= new TDAPilaImpl();
		TDACola colaAux = new TDAColaImpl();
		
		pilaAux.inicializar();
		colaAux.inicializar();
		while (!cola.colaVacia()){
			colaAux.acolar(cola.primero());
			pilaAux.apilar(cola.primero());
			cola.desacolar();
		}
		
		while (!pilaAux.pilaVacia()){
			if (colaAux.primero() != pilaAux.tope())
				return false;
			else{
				colaAux.desacolar();
				pilaAux.desapilar();
			}
		}
		return true;
	}
	
	public boolean esInversa(TDACola cola1, TDACola cola2){
		
		TDAPila pilaAux = new TDAPilaImpl();
		
		pilaAux.inicializar();
		while (!cola1.colaVacia()){
			pilaAux.apilar(cola1.primero());
			cola1.desacolar();
		}
		
		while (!pilaAux.pilaVacia()){
			if (cola2.primero() != pilaAux.tope())
				return false;
			else{
				cola2.desacolar();
				pilaAux.desapilar();
			}
		}
		return true;
	}
	
}
